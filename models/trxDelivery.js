module.exports = (sequelize, DataTypes) => {
  const trxDelivery = sequelize.define(
    "trxDelivery",
    {
      econnoteNumber: {
        type: DataTypes.STRING(20),
        allowNull: false,
        primaryKey: true,
        field: "econnote_number",
      },
      deliveryDate: {
        type: DataTypes.DATE,
        allowNull: false,
        field: "delivery_date",
      },
      originCityCode: {
        type: DataTypes.STRING(10),
        allowNull: false,
        field: "origin_city_code",
      },
      destinationCityCode: {
        type: DataTypes.STRING(10),
        allowNull: false,
        field: "destination_city_code",
      },
      transportType: {
        type: DataTypes.STRING(10),
        allowNull: false,
        field: "transport_type",
      },
      productId: {
        type: DataTypes.STRING(10),
        allowNull: false,
        field: "product_id",
      },
      shipperCustomerId: {
        type: DataTypes.STRING(10),
        allowNull: false,
        field: "shipper_customer_id",
      },
      shipperAddress: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: "shipper_address",
      },
      shipperCityCode: {
        type: DataTypes.STRING(10),
        allowNull: false,
        field: "shipper_city_code",
      },
      shipperPhone: {
        type: DataTypes.STRING(20),
        allowNull: false,
        field: "shipper_phone",
      },
      shipperZipCode: {
        type: DataTypes.STRING(10),
        allowNull: false,
        field: "shipper_zip_code",
      },
      shipperSpecialInstruction: {
        type: DataTypes.STRING(100),
        allowNull: true,
        field: "shipper_special_instruction",
      },
      consigneeName: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: "consignee_name",
      },
      consigneeAddress: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: "consignee_address",
      },
      consigneeCityCode: {
        type: DataTypes.STRING(10),
        allowNull: false,
        field: "consignee_city_code",
      },
      consigneePhone: {
        type: DataTypes.STRING(20),
        allowNull: false,
        field: "consignee_phone",
      },
      consigneZipCode: {
        type: DataTypes.STRING(10),
        allowNull: false,
        field: "consigne_zip_code",
      },
      consigneeOtherNote: {
        type: DataTypes.STRING(100),
        allowNull: true,
        field: "consignee_other_note",
      },
      shippingCost: {
        type: DataTypes.DECIMAL(15, 3),
        allowNull: false,
        field: "shipping_cost",
      },
      otherCost: {
        type: DataTypes.DECIMAL(15, 3),
        allowNull: true,
        field: "other_cost",
      },
      insuranceCost: {
        type: DataTypes.DECIMAL(15, 3),
        allowNull: false,
        field: "insurance_cost",
      },
      insuranceAdminCost: {
        type: DataTypes.DECIMAL(15, 3),
        allowNull: true,
        field: "insurance_admin_cost",
      },
      totalCost: {
        type: DataTypes.DECIMAL(15, 3),
        allowNull: false,
        field: "total_cost",
      },
      status: {
        type: DataTypes.STRING(10),
        allowNull: false,
        field: "status",
      },
      remarks: {
        type: DataTypes.STRING(100),
        allowNull: true,
        field: "remarks",
      },
      void: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: false,
        field: "void",
      },
      voidReason: {
        type: DataTypes.STRING(100),
        allowNull: true,
        field: "void_reason",
      },
      voidDate: {
        type: DataTypes.DATE,
        allowNull: true,
        field: "void_date",
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        field: "created_at",
      },
      createdBy: {
        type: DataTypes.STRING(20),
        allowNull: false,
        fiel: "created_by",
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
        field: "updated_at",
      },
      updatedBy: {
        type: DataTypes.STRING(20),
        allowNull: true,
        field: "updated_by",
      },
    },
    { tableName: "trx_delivery", timestamps: true }
  );
  return trxDelivery;
};
