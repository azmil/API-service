"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("trx_delivery", {
      econnote_number: {
        type: Sequelize.STRING(20),
        allowNull: false,
        primaryKey: true,
      },
      delivery_date: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      origin_city_code: {
        type: Sequelize.STRING(10),
        allowNull: false,
      },
      destination_city_code: {
        type: Sequelize.STRING(10),
        allowNull: false,
      },
      transport_type: {
        type: Sequelize.STRING(10),
        allowNull: false,
      },
      product_id: {
        type: Sequelize.STRING(10),
        allowNull: false,
      },
      shipper_customer_id: {
        type: Sequelize.STRING(10),
        allowNull: false,
      },
      shipper_address: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      shipper_city_code: {
        type: Sequelize.STRING(10),
        allowNull: false,
      },
      shipper_phone: {
        type: Sequelize.STRING(20),
        allowNull: false,
      },
      shipper_zip_code: {
        type: Sequelize.STRING(10),
        allowNull: false,
      },
      shipper_special_instruction: {
        type: Sequelize.STRING(100),
        allowNull: true,
      },
      consignee_name: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      consignee_address: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      consignee_city_code: {
        type: Sequelize.STRING(10),
        allowNull: false,
      },
      consignee_phone: {
        type: Sequelize.STRING(20),
        allowNull: false,
      },
      consigne_zip_code: {
        type: Sequelize.STRING(10),
        allowNull: false,
      },
      consignee_other_note: {
        type: Sequelize.STRING(100),
        allowNull: true,
      },
      shipping_cost: {
        type: Sequelize.DECIMAL(15, 3),
        allowNull: false,
      },
      other_cost: {
        type: Sequelize.DECIMAL(15, 3),
        allowNull: true,
      },
      insurance_cost: {
        type: Sequelize.DECIMAL(15, 3),
        allowNull: false,
      },
      insurance_admin_cost: {
        type: Sequelize.DECIMAL(15, 3),
        allowNull: true,
      },
      total_cost: {
        type: Sequelize.DECIMAL(15, 3),
        allowNull: false,
      },
      status: {
        type: Sequelize.STRING(10),
        allowNull: false,
      },
      remarks: {
        type: Sequelize.STRING(100),
        allowNull: true,
      },
      void: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
        defaultValue: false,
      },
      void_reason: {
        type: Sequelize.STRING(100),
        allowNull: true,
      },
      void_date: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      created_by: {
        type: Sequelize.STRING(20),
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      updated_by: {
        type: Sequelize.STRING(20),
        allowNull: true,
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("trx_delivery");
  },
};
